public class ReducedFraction {
    
    /* =========================== Свойства =============================== */
 
    /* ---------------------- Числитель и знаменатель --------------------- */
    
    private int Numerator;
    private int Denominator;
    public int getNumerator(){
        
        return this.Numerator;
    }
    public int getDenominator(){
        
        return this.Denominator;
    }
    
    public void setNumerator(int numerator){
        this.Numerator = numerator;
    }
    
    public void setDenominator(int denominator){
        if (denominator == 0) 
            {
                throw new DenZeroException("Denominator is 0");
            }
        else 
            this.Denominator = denominator;
    }
    public ReducedFraction FracsAdittion(ReducedFraction other) {
        return new ReducedFraction(this.Numerator * other.Denominator 
                + other.Numerator * this.Denominator, this.Denominator 
                        * other.Denominator);
    }

     /** Деление двух дробей.
    * 
    */ 
    public ReducedFraction FracsDiv(ReducedFraction other) {
        return new ReducedFraction(this.Numerator * other.Denominator, 
                other.Numerator * this.Denominator);
    }
}